/**
 * ISBAN México - (c) Banco Santander Central Hispano
 *
 * Creado el 08/01/2018 13:51:02 (dd/mm/aaaa hh:mm)
 *
 * Copyright (C) ISBAN All rights reserved. Todos los derechos reservados.
 *
 */
package mx.isban.tfcln.fol.config;

import javax.sql.DataSource;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import mx.isban.tfcln.fol.util.CodesDb;
/**
 * Clase CodeConfiguration
 * @author juan.perezr
 *
 */
@Configuration
public class CodeConfiguration {


    private static final Logger LOGGER = LoggerFactory.getLogger(CodeConfiguration.class);
    
    @Autowired
    private CodeConfigProperties codeConfigProperties;
    
    /**
     * Bean encargado de administrador los codigos a traves de archivos properties,
     * se extiende la funcionalidad para lectura de mensajes
     * 
     * @param dataSource
     *            origen de datos
     * @return instancia de clase para ser usada en lectura de codigos
     */
    @Bean
    @Autowired
    public CodesDb codes(DataSource dataSource) {
        LOGGER.info("Creando bean de Codes. Para app --> {} y modulo --> {} ", codeConfigProperties.getAppId(), codeConfigProperties.getModuleId());
        return new CodesDb(codeConfigProperties.getAppId(),  codeConfigProperties.getModuleId(), dataSource);
    }
}

