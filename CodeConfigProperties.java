package mx.isban.tfcln.fol.config;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

 /**
  * <b>SwaggerConfigProperties</b>
  * <p>
  * Clase donde se guardan ciertas reglas de configuaracion
  * de un archivo .yml <br>
  * A nivel de: app.api
  * </p>
  * 
  * @author juan.perezr
  *
  */
@Configuration
@ConfigurationProperties(prefix = "app")
@Validated
public class CodeConfigProperties {

    /**
     * Identifiacador unico de la aplicacion.
     */
    @NotEmpty
    @NotBlank
    @Length(min = 1)
    private String appId;

    /**
     * Identificador unico del modulo de la aplicacion.
     */
    @NotEmpty
    @NotBlank
    @Length(min = 1)
    private String moduleId;
    
    
    /**
     * Identificador unico del modulo de la aplicacion.
     */
    @NotEmpty
    @NotBlank
    @Length(min = 1)
    private String pistasUrl;

    /**
     * @return the appId
     */
    public String getAppId() {
        return appId;
    }

    /**
     * @param appId the appId to set
     */
    public void setAppId(String appId) {
        this.appId = appId;
    }

    /**
     * @return the moduleId
     */
    public String getModuleId() {
        return moduleId;
    }

    /**
     * @param moduleId the moduleId to set
     */
    public void setModuleId(String moduleId) {
        this.moduleId = moduleId;
    }

     /**
      * @return Url de pistas.
      */
	public String getPistasUrl() {
		return pistasUrl;
	}

     /**
      * @param pistasUrl URL de pistas.
      */
	public void setPistasUrl(String pistasUrl) {
		this.pistasUrl = pistasUrl;
	}
}
